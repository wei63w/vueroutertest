import { createApp } from 'vue'
import App from './App.vue'
// import VueRouter from 'vue-router'
import routes from './router/index'
// import vueConfig from 'vue.config'
// import Vue from 'vue'
// import VueRouter from 'vue-router'

// Vue.use(VueRouter)


// const router = new VueRouter({
//     routes
// })


createApp(App).use(routes).mount('#app')
