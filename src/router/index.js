import { createRouter, createWebHashHistory } from "vue-router";
// import VueRouter from 'vue-router'
import HomePage from '../views/HomePage.vue';


const routes = [
    {
        path:"/",
        name:"HomePage",
        component:HomePage
    },
    {
        path:"/home",
        name:"HomePage",
        component:HomePage
    },
    {
        path:"/home/:id",
        name:"HomePage",
        component:HomePage
    },
    {
        path:"/error",
        component:() => import("../views/ErrorPage.vue")
    },
    {
        path:"/notfound",
        component:() => import("../views/NotFound.vue")
    },
    {
        path:"/:catchAll(.*)",
        redirect: '/notfound',
    },
]


//createWebHashHistory hash 模式
//内部传递的实际 URL 之前使用了一个哈希字符（#）。由于这部分 URL 从未被发送到服务器，所以它不需要在服务器层面上进行任何特殊处理。不过，它在 SEO 中确实有不好的影响

//createWebHistory HTML5 模式
// https://example.com/user/id

const router = createRouter(
    {
        history: createWebHashHistory(),
        routes
    }
)

//全局前置守卫
//vue-router 提供的导航守卫主要用来通过跳转或取消的方式守卫导航
//to: 即将要进入的目标 用一种标准化的方式
//from: 当前导航正要离开的路由 用一种标准化的方式
router.beforeEach((to, from ) => {
    
    //处理登录操作等
    
    // 返回 false 以取消导航, 可以进行拦截处理等
    // return false
})

//全局解析守卫
//每次导航时都会触发，但是确保在导航被确认之前，同时在所有组件内守卫和异步路由组件被解析之后，解析守卫就被正确调用
//获取数据或执行任何其他操作（如果用户无法进入页面时你希望避免执行的操作）的理想位置

//例如访问自定义 meta 属性requiresCamera 的路由
router.beforeResolve(async to => {
    if (to.meta.requiresCamera) {
      try {
        // await askForCameraPermission()
      } catch (error) {
        // if (error instanceof NotAllowedError) {
        //   // ... 处理错误，然后取消导航
        //   return false
        // } else {
        //   // 意料之外的错误，取消导航并把错误传给全局处理器
        //   throw error
        // }
      }
    }
  })
//全局后置钩子
//对于分析、更改页面标题、声明页面等辅助功能以及许多其他事情都很有用
router.afterEach((to, from) => {
    // sendToAnalytics(to.fullPath)
})

export default router;


